package swmongo

import (
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

type MongoOperator struct {
	monClient *mongo.Client
	hostName  string
	connPort  int
	// lastCall  string
	// fatalMsg  string
	// handler   func(ctx context.Context, cmd bson.D) ([]interface{}, error)
}

type DocResponse struct {
	Doc    bson.D
	Length int
	RtnIDs []string
}
